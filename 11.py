seconds=5000
import math
def convert_time(seconds):
    d=(((seconds/60)/60)/24)
    day=math.floor(d)
    ds=(d-day)*24
    hour=math.floor(ds)
    m=(ds-hour)*60
    min=math.floor(m)
    s=(m-min)*60
    sec=math.floor(s)
    return day,hour,min,sec

day,hour,min,sec=convert_time(seconds)
print(day,"\n",hour,"\n",min,"\n",sec)
